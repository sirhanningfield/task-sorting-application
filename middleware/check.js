const taskAssignmentController = require('../controllers/taskAssignmentController');

module.exports = function (req,res,next) {
    // check if incoming request already has a body
    if (!isEmpty(req.body)) {
        next();
    }
    // check if all tables have value
    taskAssignmentController.check((err,result)=>{
        if (err) {
            return res.status(400).json({
                status:0,
                msg : err
            })
        }else{
            next()
        }
    })
}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}