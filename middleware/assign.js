
const taskAssignmentController = require('../controllers/taskAssignmentController');

module.exports = function (req,res,next) {
    if (!isEmpty(req.body)) {
        next();
    }
    taskAssignmentController.taskAssignment((err,result)=>{
        if (err) {
            return res.status(400).json({
                status:0,
                msg : err
            })
        }else{
            req.body = result
            next()
        }
    })
}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}