const db = require('../models');

exports.assignTasksToDatabase = function (req,res) {
    var data = req.body;
    // assign data to backend
    for(var key in data){
        // validate data here , then send to database  
        var tasks = data[key].tasks
        
        if (!Array.isArray(tasks) || !key.includes("TEAM_")) {
            return res.status(401).json({
                status:0,
                msg : "tasks assigned must be array"
            })
        }
        data[key].tasks.forEach((task,index,array) => {
            var rowData = {"TEAM_ID" : key , "TASK_ID" : task}
            db.assignment_result.create(rowData);
        });
  
    }
    res.status(201).json({
        status : 1,
        msg : "data stored succesfully"
    })
}