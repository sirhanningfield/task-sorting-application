const db = require('../models')
const logger = require('../logger').logger;

exports.check = function (callback) {
    db.team.findOne({ raw : true}).then((teams)=>{
        if (teams) {
            db.team_skill.findOne({ raw : true, attributes:['TEAM_ID']}).then((team_skill)=>{
                if (team_skill) {
                    db.task.findOne({ raw : true, attributes:['TASK_ID']}).then((task)=>{
                        if (task) {
                            callback(null, true);

                        }else{
                            callback(`task table is empty`,null)
                        }
                    })
                }else{
                    callback(`team_skill table is empty`,null)
                }
            })
        }else{
            callback(`team table is empty`,null)
        }
    })
}

exports.taskAssignment = function (callback) {
    let result = {};
    // TO DO : get all the teams from the table and make result object based on the teams
    db.team.findAll({
        raw: true,
    }).then((teams)=>{
        if(!teams){
            callback("Teams not found",null);
            return
        }
        teams.forEach( team => {
            result[team.TEAM_ID]= {
                tasks : [],
                count : 0
            }; 
        })
        return result
    }).then((result)=>{
       
        // TO DO : get all the tasks from task  table
        db.task.findAll({raw:true}).then(async (task)=>{
            if (!task) {
                callback(`No tasks found`,null)
            }
            // TO DO : for each skill , check all the teams that have the skill from team_skill table
            task.forEach((task,index,array) => {
                db.team_skill.findAll({
                    raw :true,
                    attributes: ['TEAM_ID'],
                    where : {
                        SKILL : task.SKILL
                    }
                }).then((teams)=>{
                    teamSkills = {};
                    teams.forEach(team => {
                        teamSkills[team.TEAM_ID] = result[team.TEAM_ID].count
                        
                    })
                    // sort the taskSkill array based on the lowest count of the team tasks in result
                    sortedArray = sortProperties(teamSkills);

                    // assign the task to the first team in the sorted taskSkill array
                    result[sortedArray[0][0]].tasks.push(task.TASK_ID)
                    result[sortedArray[0][0]].count += 1
                    if (index === (array.length -1)) {
                        callback(null,result);
                    }
                })
                
            })
            
            
            
        })
    });

    

    function sortProperties(obj)
{
    // convert object into array
        var sortable=[];
        for(var key in obj)
            if(obj.hasOwnProperty(key))
                sortable.push([key, obj[key]]); // each item is an array in format [key, value]
        
        // sort items by value
        sortable.sort(function(a, b)
        {
        return a[1]-b[1]; // compare numbers
        });
        return sortable; // array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
}
    

   

    

    // TO DO : 
}