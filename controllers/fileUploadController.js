
const csvToJson = require('csvtojson');
const csv = csvToJson();
const db = require('../models');
const fs = require('fs');


const logger = require('../logger').logger;


exports.processFileUpload = async function(callback) {
    if ( process.env["FILE_NAME"] != undefined ) {
        var filePath = process.env["FILE_NAME"];
        
        logger.log('info', `Worker ${process.pid} started running ${filePath}`);
        
        var tableName = this.getTableName(filePath) // Get table name from the uploaded file (names are the same as the files)
        var databaseObject = this.getDatabaseObject(tableName)
        if (!databaseObject) {
            logger.log('error', `Invalid file name`);
            deleteFile(filePath)
            callback("Error",null)
        }
        
        // Read the CSV file data into json
        this.readCsvData(filePath)
            .then(async(jsonData)=>{
                
                // write json data into tables
                $result = await addDataToTable(jsonData,databaseObject);
                
            }).then(()=>{
                logger.log('info', `Worker ${process.pid} finsihed processing ${filePath}`);
                deleteFile(filePath);
                callback(null, true);
            })
    }
}

addDataToTable = function (jsonData,databaseObject) {
    return new Promise((resolve,reject)=>{
        jsonData.forEach(line => {
            addLineToTable(databaseObject,line)
        });
        resolve()
    })
}

async function deleteFile(filePath){
    fs.unlink(filePath, function (err) {
        if (err) throw err;
        logger.log('info', `${filePath} deleted.`);
    }); 
}

exports.getDatabaseObject = function(tableName) {
    switch (tableName) {
        case 'team':
            return db.team
            break;
        case 'task':
            return db.task
            break;
        case 'team_skill':
            return db.team_skill
            break;
        default:
            return false
            break;
    }
}

function addLineToTable(databaseObject,line) {
    databaseObject.create(line)
}

exports.getTableName = function (filePath) {
    console.log(filePath);
    return filePath.replace(/^.*[\\\/]/, '').split('.')[0]
}

exports.readCsvData = function(filePath) {
    // console.log(csv.fromFile(filePath));
    return csv.fromFile(filePath)
}
