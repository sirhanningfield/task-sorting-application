'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('tasks', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },TASK_ID: {
          type: Sequelize.STRING
        }, SKILL: {
          type: Sequelize.STRING
        },createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        }, updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('tasks');
  }
};