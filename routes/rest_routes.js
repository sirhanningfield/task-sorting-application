const express = require('express');
const router = express.Router();
const restController = require('../controllers/restController');
const check = require('../middleware/check')
const assign = require('../middleware/assign')

router.post('/assignment_result',check,assign,restController.assignTasksToDatabase)

module.exports = router;
