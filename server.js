const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/rest_routes');
const chokidar = require('chokidar');
const cluster = require('cluster');
const fs = require('fs');
const fileUploadController = require('./controllers/fileUploadController');
const taskAssignmentController = require('./controllers/taskAssignmentController');
const db = require('./models');
const logger = require('./logger').logger;



const app = express();

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

app.use('/rest',routes);

var workerEnv = {};
watcher = chokidar.watch('files/**/*.csv', { 
    ignored:/(^|[\/\\])\../,
    persistent: true,
    ignoreInitial: true,
    alwaysState: true}
);

if (cluster.isMaster) {
    var port_number = process.env.PORT || 3000;

    app.listen(port_number, function(){
        console.log(`Listening on port ${port_number}`)
    });
    app.get('/',function (req, res) { // for testing purpose only
        res.send('Hello World!');
    })
    listenFileUpload();
}else {
    fileUploadController.processFileUpload(
        function(err,result) {
            if (result) {
                // check if all tables have data, then assign tasks
                taskAssignmentController.check((err,result)=>{
                    if (err) {
                        logger.log('error', `${err}`);
                    }else{
                        taskAssignmentController.taskAssignment((err,result)=>{
                            if (result) {
                                var data = JSON.stringify(result)
                                logger.log('info',`Assignment Results : ${data}`);
                                return result
                            } else if(err) {
                                logger.log('error', `${err}`);
                            }
                        })
                    }
                });
            }
        }
    )
}


function listenFileUpload() {
    console.log(`Master ${process.pid} is running...`);
    watch();
}

function watch() {
    watcher.on('add', path => {
        workerEnv["FILE_NAME"] = path;
        createProcess(workerEnv)
    })
}

function createProcess(workerEnv){
    if(workerEnv["FILE_NAME"]){
        cluster.fork(workerEnv);
    }
}




