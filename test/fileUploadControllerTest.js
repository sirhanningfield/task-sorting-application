const assert = require('chai').assert;
const fileUploadController = require('../controllers/fileUploadController');
const db = require('../models');
const csvToJson = require('csvtojson');
const csv = csvToJson();

describe('FileUpload',()=>{
    it('should return the model object',()=>{
        assert.equal(fileUploadController.getDatabaseObject('random'),false);
    });
})

describe('FileUpload',()=>{
    it('should return the model object',()=>{
        assert.equal(fileUploadController.getDatabaseObject('team'),db.team);
    });
})

describe('FileUpload',()=>{
    it('should return the model object',()=>{
        assert.equal(fileUploadController.getDatabaseObject('task'),db.task);
    });
})

describe('FileUpload',()=>{
    it('should return table name from file name',()=>{
        assert.equal(fileUploadController.getTableName("files\\task.csv"),'task');
    });
})


