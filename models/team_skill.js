'use strict';
module.exports = (sequelize, DataTypes) => {
  const team_skill = sequelize.define('team_skill', {
    TEAM_ID: DataTypes.STRING,
    SKILL: DataTypes.STRING
  }, {});
  team_skill.associate = function(models) {
    // associations can be defined here
    // team_skill.belongsTo(models.task,{foreignKey : 'TEAM_ID'})
  };
  return team_skill;
};