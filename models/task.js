'use strict';
module.exports = (sequelize, DataTypes) => {
  const task = sequelize.define('task', {
    TASK_ID: DataTypes.STRING,
    SKILL: DataTypes.STRING
  }, {});
  task.associate = function(models) {
    // associations can be defined here
  };
  return task;
};