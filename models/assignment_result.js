'use strict';
module.exports = (sequelize, DataTypes) => {
  const assignment_result = sequelize.define('assignment_result', {
    TEAM_ID: DataTypes.STRING,
    TASK_ID: DataTypes.STRING
  }, {});
  assignment_result.associate = function(models) {
    // associations can be defined here
  };
  return assignment_result;
};